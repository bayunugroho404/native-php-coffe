<?php
    // session start
if(!empty($_SESSION)){ }else{ session_start(); }
require 'proses/panggil.php';
$id = strip_tags($_GET['id']);
$hasil = $proses->tampil_data_id('product','id',$id);
?>

<!DOCTYPE HTML>
<html>
<head>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

 <title>Coffe Terbaik</title>
 <!-- BOOTSTRAP 4-->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
 <!-- DATATABLES BS 4-->
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" />
 <!-- Font Awesome -->
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

 <!-- jQuery -->
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
 <!-- DATATABLES BS 4-->
 <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
 <!-- BOOTSTRAP 4-->
 <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

</head>
<body style="background:#586df5;">
  <div class="container">
   <div class="row">
    <div class="col-lg-12">

      <?php if(!empty($_SESSION['ADMIN'])){?>
        <br/>
        <span style="color:#fff";>Selamat Datang, <?php echo $sesi['nama_pengguna'];?></span>
        <a href="logout.php" class="btn btn-danger btn-md float-right"><span class="fa fa-sign-out"></span> Logout</a>
        <br/><br/>

        <center> 
          <span style="padding: 10px;" class="badge badge-pill badge-light">Detail Pesanan : <?php echo $hasil['nama']; ?>, Harga Satuan : Rp. <?php echo $hasil['harga']; ?></span>
          <div class="col-6">
            <br>

            <div class="card">
              <div class="card-header">
                <center><h4 class="card-title">Form Pesanan</h4></center>            
              </div>
              <div class="card-body">
               <div class="row">
                <div class="col-md-12 mx-auto">
                  <div class="myform form ">
                   <form action="proses/crud.php?aksi=pesan" method="post" name="login">
                    <input hidden type="text" name="user_id"  value="<?php echo $_SESSION['user_id']?>" class="form-control my-input" id="user_id">
                    <div class="form-group">
                     <input type="text" name="name"  class="form-control my-input" id="name" placeholder="Name">
                   </div>
                   <div class="form-group">
                     <input type="text" name="telepon"  class="form-control my-input" id="telepon" placeholder="telepon">
                   </div>
                   <div class="form-group">
                     <input type="number" name="qty" id="qty"  class="form-control my-input" placeholder="qty">
                   </div>
                   <div class="text-center ">
                     <button type="submit" class="btn btn-warning">&nbsp&nbsp Submit &nbsp&nbsp</button>
                   </div>
                   <br>
                 </form>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div></center>


   <?php }else{?>
    <br/>
    <div class="alert alert-info">
      <h3>Silahkan Login Terlebih Dahulu !</h3>
      <hr/>
      <p><a href="login.php">Login Disini</a></p>
    </div>
  <?php }?>
</div>
</div>
</div>
<script>
  $('#mytable').dataTable();
</script>
</body>
</html>
