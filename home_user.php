<?php
    // session start
if(!empty($_SESSION)){ }else{ session_start(); }
require 'proses/panggil.php';
?>

<!DOCTYPE HTML>
<html>
<head>
 <title>Bootstrap Example</title>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
 <style>
  /* Make the image fully responsive */
  .carousel-inner img {
    height: auto;
    width: 100%;
  }
</style>
</head>
<body style="background:#586df5;">
  <div class="container">
   <div class="row">
    <div class="col-lg-12">


      <!-- jika berhasil login -->
      <?php if(!empty($_SESSION['ADMIN'])){?>
        <br>
        <span style="color:#fff";>Selamat Datang, <?php echo $sesi['nama_pengguna'];?></span>
        <a href="logout.php" class="btn btn-danger btn-md float-right"><span class="fa fa-sign-out"></span> Logout</a>
        <br/><br/>

        <div id="demo" class="carousel slide" data-ride="carousel">
          <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
          </ul>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="https://ae01.alicdn.com/kf/HTB1dyCgkcnI8KJjSsziq6z8QpXal/Diamond-Embroidery-Painting-Coffe-Time-5D-Full-Drill-Diamond-Cross-Stitch-Pasted-Painting-DIY-Home-Decora.jpg_Q90.jpg_.webp" alt="Los Angeles" width="1100" height="500">
              <div class="carousel-caption">
                <h3>Los Angeles</h3>
                <p>We had such a great time in LA!</p>
              </div>   
            </div>
            <div class="carousel-item">
              <img src="https://img.okezone.com/content/2019/06/09/298/2064765/aroma-kopi-indonesia-semakin-harum-di-world-of-coffe-jerman-C8ygzWfm8s.jpg" alt="Chicago" width="1100" height="500">
              <div class="carousel-caption">
                <h3>Chicago</h3>
                <p>Thank you, Chicago!</p>
              </div>   
            </div>
            <div class="carousel-item">
              <img src="https://ae01.alicdn.com/kf/HTB1ka9bSXXXXXX0XFXXq6xXFXXXy/Lukisan-Berlian-Ukuran-Besar-Rhinestones-Bordir-Coffe-Dekorasi-Dekoratif-Biji-Kopi.jpg" alt="New York" width="1100" height="500">
              <div class="carousel-caption">
                <h3>New York</h3>
                <p>We love the Big Apple!</p>
              </div>   
            </div>
          </div>
          <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
          </a>
          <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
          </a>
        </div>
        <br>
        <div class="row"> 
          <?php
          $data = $proses->get_product('product');
          foreach($data as $product){
            ?>
            <br>
            <br>
            <!-- info prododuct -->
            <div class="col-4">
             <div class="card">
              <div class="card-header">
                <h4 class="card-title"><?php echo $product['nama'];?></h4>
              </div>
              <div class="card-body">
                <div class="card">
                  <img class="card-img-top" src="<?php echo $product['gambar'];?>" style="height: 200px" alt="Card image cap">
                  <div class="card-body">
                    <h5 class="card-title">Rp.<?php echo $product['harga'];?></h5>
                    <p class="card-text"><?php echo $product['deskripsi'];?></p>
                    <a href="pesan.php?id=<?php echo $product['id'];?>" class="btn btn-primary">Pesan Sekarang</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
      </div>

    <?php }else{?>
      <br/>
      <div class="alert alert-info">
        <h3>Silahkan Login Terlebih Dahulu !</h3>
        <hr/>
        <p><a href="login.php">Login Disini</a></p>
      </div>
    <?php }?>
  </div>
</div>

</div>
<br>
<div class="container" >
    <div class="row">
        <div class="col-md-8">
          <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="
          https://www.google.com/maps/embed/v1/place?q=Cyberborg%20Bodyart%2C%20Bangka%2C%20Jakarta%2C%20Indonesia&key=AIzaSyCGz8WzqxQw1OwHWey3LCTjqKFG9feCxP4"></iframe>
      </div>
      
        <div class="col-md-4">
        <h2>CONTACT US</h2>
        <address>
          <strong>CYBERBORG BODYART</strong><br>
          Jl.Kemang 1 No.2B<br>
          Bangka, Mampang Prapatan<br>
          East Jakarta, DKI<br>
          Indonesia<br>
          12730<br>
          <abbr title="Phone">P:</abbr> (+62) 856-1891-791
        </address>
      </div>
    </div>
</div>
<br>
<script>
  $('#mytable').dataTable();
</script>
</body>
</html>
